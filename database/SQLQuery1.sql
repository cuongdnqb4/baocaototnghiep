﻿CREATE DATABASE QL_internet
ON PRIMARY(
NAME=QL_INTERNET_DATA,
FILENAME='C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\QL_internet.MDF',
SIZE=5MB,
MAXSIZE=UNLIMITED,
FILEGROWTH=5MB
)
LOG ON(
NAME=QL_INTERNET_LOG,
FILENAME='C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\QL_internet.LDF',
SIZE=5MB,
MAXSIZE=UNLIMITED,
FILEGROWTH=5MB
)
go
use QL_internet
go

--Bảng gói dịch vụ
create table Dangnhap
(
Username nvarchar(50) primary key,
Matkhau nvarchar (50),
Quyenhan nvarchar(50),
Tennhanvien nvarchar(50),
Sodienthoai nvarchar(50),
)
insert dbo.Dangnhap values('admin1','admin1','user',N'Cường','113')
insert dbo.Dangnhap values('admin','admin','admin',N'Cường','113')

create table Banggoidichvu
(
MaDV int identity(1,1) primary key not null,
TenDV nvarchar (50) Unique ,
Giatien int
)
select Giatien from BangKH a,Banggoidichvu b where a.MaKH=1 and a.GoiDV=b.MaDV
--Bảng kiểu nộp trước

create table Bangkieunoptruoc
(
Makieunop int identity(1,1) primary key not null,
TenGoi nvarchar (50) Unique,
Sothang int
)

--Bảng khách hàng 

create table BangKH
(
MaKH int identity(1,1) primary key not null,
TenKH nvarchar(50),
DiaChi nvarchar(150),
SoDT varchar(20),
GoiDV int not null foreign key references Banggoidichvu(MaDV),
Kieunop int not null foreign key references Bangkieunoptruoc(Makieunop),
Ngaybatdau date,
Ngayketthuc date
)

--Bảng Hóa đơn 

create table BangHD
(
MaHD int identity(1,1) not null,
MaKH int not null foreign key references BangKH(MaKH),
NgayDongTiep date,
Tiendanop nvarchar(50)
)

--Nhập dữ liệu
--bảng gói dịch vụ
insert Banggoidichvu(TenDV,Giatien) values ('10M',100000)
insert Banggoidichvu(TenDV,Giatien) values ('16M',150000)
insert Banggoidichvu(TenDV,Giatien) values ('25M',200000)
insert Banggoidichvu(TenDV,Giatien) values ('35M',350000)
insert Banggoidichvu(TenDV,Giatien) values ('40M',400000)
--bảng kiểu nộp trước
insert Bangkieunoptruoc values (N'Gói 6 tháng',6)
insert Bangkieunoptruoc values (N'Gói 12 tháng',12)
--bảng khách hàng
insert BangKH  values (N'Nguyễn Thị Ngọc Trâm',N'Bình Dương','01674116779','1','1','2017/05/12','2017/11/12')
insert BangKH  values (N'Nguyễn Thu Thủy',N'Daklak','01674444120','4','2','2017/05/12','2017/11/12')
insert BangKH  values (N'Phạm Nhật Tân',N'Bình Dương','01658619373','3','1','2017/05/12','2017/11/12')
insert BangKH  values (N'Nguyễn Quốc Cường',N'Quảng Bình','01667887486','3','2','2017/05/12','2017/11/12')
insert BangKH values (N'Nguyễn Đức Biên',N'Bình Phước','01674116559','1','1','2017/05/12','2017/11/12')
--Bảng Hóa đơn
insert BangHD(MaKH,NgayDongTiep,Tiendanop) values ('1','2017/10/12','600000')
insert BangHD(MaKH,NgayDongTiep,Tiendanop) values ('2','2019/09/10','900000')
insert BangHD(MaKH,NgayDongTiep,Tiendanop) values ('3','2018/12/02','12000000')
insert BangHD(MaKH,NgayDongTiep,Tiendanop) values ('4','2017/02/04','12000000')
insert BangHD(MaKH,NgayDongTiep,Tiendanop) values ('5','2019/03/05','600000')

--Đăng nhập
select SUM(Convert(int,Tiendanop)) from BangHD
--truy vấn
--BM1
select MaKH,TenKH,SoDT,DiaChi,MaDV,Ngaybatdau,Giatien,Ngayketthuc,Makieunop
from BangKH left join Banggoidichvu on BangKH.GoiDV=Banggoidichvu.MaDV right join Bangkieunoptruoc on BangKH.Kieunop=Bangkieunoptruoc.Makieunop
--BM2
select TenKH,Ngaybatdau,NgayDongTiep,Tiendanop,TenDV,Tenkieunop,DiaChi
from BangKH left join Banggoidichvu on BangKH.GoiDV=Banggoidichvu.MaDV right join BangHD on BangKH.MaKH=BangHD.MaKH right join Bangkieunoptruoc on BangKH.Kieunop=Bangkieunoptruoc.Makieunop
--BM3
select TenKH,GoiDV,Tiendanop,Ngaybatdau,Ngayketthuc,NgayDongTiep
from BangKH left join Banggoidichvu on BangKH.GoiDV=Banggoidichvu.MaDV right join BangHD on BangKH.MaKH=BangHD.MaKH right join Bangkieunoptruoc on BangKH.Kieunop=Bangkieunoptruoc.Makieunop

select MaDV	from Banggoidichvu

select Giatien from Banggoidichvu
 
select Makieunop from Bangkieunoptruoc
 
select MaKH from BangKH

select * from BangKH

Alter table Dangnhap add constraint ck_quyenhhan check (Quyenhan in ('admin','user'))