﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace QL_ITN
{
    public partial class InfomationHoadon : UserControl
    {
        public InfomationHoadon()
        {
            InitializeComponent();
            loadGridcontrol();
        }

        private void InfomationHoadon_Load(object sender, EventArgs e)
        {

        }
        clsDatabase m = new clsDatabase();
        private void loadGridcontrol()
        {

            DataTable dt = new DataTable("DB");
            dt.Clear();
            SqlDataAdapter adapter = new SqlDataAdapter("select MaHD as [Mã HĐ],MaKH as [Mã KH],NgayDongTiep as [Ngày đóng tiền],Tiendanop as [Số tiền đã nộp] from BangHD", Form1.strc);
            adapter.Fill(dt);
            gridControl1.DataSource = dt;
            label4.Text = gridView1.RowCount.ToString();
            m.LoadData2Label(label5, "select SUM(Convert(int,Tiendanop)) from BangHD");
            label5.Text = label5.Text + "VNĐ";

        }
    }
}
