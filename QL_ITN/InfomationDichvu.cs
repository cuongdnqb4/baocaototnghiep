﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace QL_ITN
{
    public partial class InfomationDichvu : UserControl
    {
        public InfomationDichvu()
        {
            InitializeComponent();
            loadGridcontrol();
        }
        private void loadGridcontrol()
        {

            DataTable dt = new DataTable("DB");
            dt.Clear();
            SqlDataAdapter adapter = new SqlDataAdapter("select MaDV as [Mã DV],TenDV as [Tên Dịch vụ],Giatien as [Giá tiền] from Banggoidichvu", Form1.strc);
            adapter.Fill(dt);
            gridControl1.DataSource = dt;
            Phanquyen();

        }
        private void Phanquyen()
        {
            if(Form1.role=="admin")
            {

            }
            else
            {
                
                button1.Enabled = false;
                button2.Enabled = false;
            }
        }
        clsDatabase m = new clsDatabase();
        //Tự động update khi thực thi trên pform update
        private void gridView1_RowUpdated(object sender, DevExpress.XtraGrid.Views.Base.RowObjectEventArgs e)
        {
            DataRow row;
            for (int i = 0; i <= gridView1.RowCount -1 ; i++)
            {
                row = gridView1.GetDataRow(i);
                string query = "update dbo.Banggoidichvu set TenDV=N'"+row["Tên Dịch vụ"].ToString()+"'" +
                    ",Giatien="+row["Giá tiền"].ToString()+" where MaDV="+row["Mã DV"].ToString()+" ";
                m.ThucThiSQLTheoPKN(query);
            }
            loadGridcontrol();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                DataRow row;

                row = gridView1.GetDataRow(gridView1.FocusedRowHandle);
                DialogResult dt = MessageBox.Show("Bạn có chắn chắn muốn xóa k?", "Thông báo", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
                if (dt == DialogResult.OK)
                {
                    string query = "Delete dbo.Banggoidichvu where MaDV='" + row["Mã DV"].ToString() + "'";
                    m.ThucThiSQLTheoPKN(query);
                }
                loadGridcontrol();
            }
            catch
            {
                MessageBox.Show("Không thể xóa vì Dữ liệu cần liên kết đến thông tin khách hàng");
            }
        }

        private void InfomationDichvu_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            AddDV m = new AddDV();
            m.ShowDialog();
            loadGridcontrol();
        }
    }
}
