﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace QL_ITN
{
    public partial class InfomationNV : UserControl
    {
      
        
        
        public InfomationNV()
        {
            InitializeComponent();
            Phanquyen();
            
        }
       
        private void loadGridcontrol()
        {
            if (Form1.role == "admin")
            {
                DataTable dt = new DataTable("DB");
                dt.Clear();
                SqlDataAdapter adapter = new SqlDataAdapter("select Username,Matkhau as [Mật khẩu],Quyenhan as [Quyền hạn],Tennhanvien as [Tên nhân viên],Sodienthoai as [Số điện thoại] from Dangnhap", Form1.strc);
                adapter.Fill(dt);
                gridControl1.DataSource = dt;
            }
            else
            {
                DataTable dt = new DataTable("DB");
                dt.Clear();
                SqlDataAdapter adapter = new SqlDataAdapter("select Username,Matkhau as [Mật khẩu],Quyenhan as [Quyền hạn],Tennhanvien as [Tên nhân viên],Sodienthoai as [Số điện thoại] from Dangnhap where Username='"+Form1.username+"'", Form1.strc);
                adapter.Fill(dt);
                gridControl1.DataSource = dt;
            }

        }
        private void Phanquyen()
        {
            if (Form1.role == "admin")
            {

            }
            else
            {

               
                button1.Enabled = false;
                button2.Enabled = false;
            }
        }
        private void gridView1_Click(object sender, EventArgs e)
        {
           
        }
        clsDatabase m = new clsDatabase();
        private void gridView1_RowUpdated(object sender, DevExpress.XtraGrid.Views.Base.RowObjectEventArgs e)
        {
            DataRow row;
            for (int i = 0; i <= gridView1.RowCount - 1; i++)
            {
                row = gridView1.GetDataRow(i);
                string query = "update dbo.Dangnhap set Matkhau='"+row["Mật khẩu"].ToString() +"',Tennhanvien=N'"+row["Tên nhân viên"].ToString()+"'," +
                    "Sodienthoai='"+row["Số điện thoại"].ToString() + "'  where Username='" + row["Username"].ToString()+"'";
                m.ThucThiSQLTheoKetNoi("update dbo.Dangnhap set Matkhau='aaa' where Username='admin1'");
            }
            loadGridcontrol();
        }

        private void InfomationNV_Load(object sender, EventArgs e)
        {
            loadGridcontrol();
        }

        private void gridView1_RowUpdated_1(object sender, DevExpress.XtraGrid.Views.Base.RowObjectEventArgs e)
        {
            DataRow row;
            for (int i = 0; i <= gridView1.RowCount - 1; i++)
            {
                row = gridView1.GetDataRow(i);
                string query = "update dbo.Dangnhap set Matkhau='" + row["Mật khẩu"].ToString() + "',Tennhanvien=N'" + row["Tên nhân viên"].ToString() + "'," +
                    "Sodienthoai='" + row["Số điện thoại"].ToString() + "'  where Username='" + row["Username"].ToString() + "'";
                m.ThucThiSQLTheoPKN(query);
            }
            loadGridcontrol();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            DataRow row;

            row = gridView1.GetDataRow(gridView1.FocusedRowHandle);
            DialogResult dt = MessageBox.Show("Bạn có chắn chắn muốn xóa k?", "Thông báo", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
            if (dt == DialogResult.OK)
            {
                string query = "Delete dbo.Dangnhap where Username='" + row["Username"].ToString() + "'";
                m.ThucThiSQLTheoPKN(query);
            }
            loadGridcontrol();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            AddNV m = new AddNV();
            m.ShowDialog();
            loadGridcontrol();
        }
    }
}
