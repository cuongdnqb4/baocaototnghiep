﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace QL_ITN
{
    public partial class InfomationKH : UserControl
    {
        public InfomationKH()
        {
            InitializeComponent();
            loadGridcontrol();
            gridView1.Columns[0].Width =50;
            gridView1.Columns[1].Width = 180;
            gridView1.Columns[3].Width = 130;
        }
        private void loadGridcontrol()
        {
            
            DataTable dt = new DataTable("DB");
            dt.Clear();
            SqlDataAdapter adapter = new SqlDataAdapter("select MaKH as [Mã KH],TenKH as [Tên khách hàng],DiaChi as" +
                " [Địa chỉ],SoDT as [Số điện thoại],GoiDV as [Gói DV],Kieunop as [Gói trả trước],Ngaybatdau as" +
                " [Ngày đăng kí],Ngayketthuc as [Ngày kết thúc] from BangKH", Form1.strc);
            adapter.Fill(dt);
            gridControl1.DataSource = dt;

        }
        clsDatabase m = new clsDatabase();
        //Tự động update khi thực thi trên pform update
        private void gridView1_RowUpdated(object sender, DevExpress.XtraGrid.Views.Base.RowObjectEventArgs e)
        {
            DataRow row;
            for(int i=0;i <= gridView1.RowCount -1;i++)
            {
                row = gridView1.GetDataRow(i);
                string query = "update dbo.BangKH set TenKH=N'" + row["Tên khách hàng"].ToString() + "',Diachi=N'" + row["Địa chỉ"].ToString() + "'" +
                    ",SoDT='" + row["Số điện thoại"] + "' where MaKH='" + row["Mã KH"].ToString() + "'";
                m.ThucThiSQLTheoPKN(query);
            }
            loadGridcontrol();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            DataRow row;

            row = gridView1.GetDataRow(gridView1.FocusedRowHandle);
            DialogResult dt = MessageBox.Show("Bạn có chắn chắn muốn xóa k?", "Thông báo", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
            if (dt == DialogResult.OK)
            {
                string query2 = "Delete dbo.BangHD where MaKH='" + row["Mã KH"].ToString() + "'";
                string query = "Delete dbo.BangKH where MaKH='" + row["Mã KH"].ToString() + "'";
                m.ThucThiSQLTheoPKN(query2);
                m.ThucThiSQLTheoPKN(query);
                
            }
            loadGridcontrol();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Giahan m = new Giahan();
            m.ShowDialog();
            loadGridcontrol();
        }
    }
}
