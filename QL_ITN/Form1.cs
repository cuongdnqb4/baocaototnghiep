﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
namespace QL_ITN
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
             strc = @"Data Source=DESKTOP-PN2KFBL\SQLEXPRESS;Initial Catalog=QL_internet;Integrated Security=True";
            SqlConnection sqlcon = new SqlConnection(strc);
            timer1.Start();
            GIaodien m = new GIaodien();
            Addcontrolstopnal(m);
            label3.Text = tennv;
            label4.Text = role;

        }
        private void moveSidePanel(Control btn)
        {
            panelSide.Top = btn.Top;
           
        }
        public static string strc = string.Empty;
        public static string username = string.Empty;
        public static string role = string.Empty;
        public static string tennv = string.Empty;
        // Thêm Usercontrol vào panel 
        private void Addcontrolstopnal(Control c)
        {
           
            c.Dock = DockStyle.Fill;
            panel4.Controls.Clear();
            panel4.Controls.Add(c);
        }
        // Khi click sẽ Add Usercontrol
        private void button7_Click(object sender, EventArgs e)
        {
            moveSidePanel(button7);
            InfomationNV m = new InfomationNV();
            Addcontrolstopnal(m);
        }

        private void button11_Click(object sender, EventArgs e)
        {
            moveSidePanel(button11);
            InfomationKH m = new InfomationKH();
            Addcontrolstopnal(m);
        }

        private void button10_Click(object sender, EventArgs e)
        {
            moveSidePanel(button10);
            DangkiHoadon m = new DangkiHoadon();
            Addcontrolstopnal(m);
        }

        private void button9_Click(object sender, EventArgs e)
        {
            moveSidePanel(button9);
            InfomationDichvu m = new InfomationDichvu();
            Addcontrolstopnal(m);
        }

        private void button8_Click(object sender, EventArgs e)
        {
            moveSidePanel(button8);
            InfomationGoi m = new InfomationGoi();
            Addcontrolstopnal(m);
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            DateTime dt = DateTime.Now;
            label5.Text = dt.ToString();
        }

        private void panel6_Paint(object sender, PaintEventArgs e)
        {
            
        }

        private void panel6_Click(object sender, EventArgs e)
        {
            GIaodien m = new GIaodien();
            Addcontrolstopnal(m);
        }

        private void button6_Click(object sender, EventArgs e)
        {
            this.Hide();
            DangNhap m = new DangNhap();
            m.ShowDialog();
        }

        private void button12_Click(object sender, EventArgs e)
        {
            moveSidePanel(button12);
            InfomationHoadon m = new InfomationHoadon();
            Addcontrolstopnal(m);
        }
    }
}
