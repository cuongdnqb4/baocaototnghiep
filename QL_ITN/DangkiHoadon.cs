﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace QL_ITN
{
    public partial class DangkiHoadon : UserControl
    {
        public DangkiHoadon()
        {
            DateTime dt = DateTime.Now;
            
            InitializeComponent();
        }

        private void DangkiHoadon_Load(object sender, EventArgs e)
        {
            Loadgoi();
            LoadGoidich();
            
        }
        string goi,dichvu,giatien,sothang = "";
        clsDatabase m = new clsDatabase();
        private void Loadgoi()
        {

            DataTable dt = new DataTable();
            dt.Clear();
            dt= m.Laybangtb("select * from Banggoidichvu",dt);
            
            foreach (DataRow row in dt.Rows) { 
            Button btn = new Button() { Name=row["MaDV"].ToString(), Width = 300, Height = 50, BackColor = Color.WhiteSmoke };
                Label tendv = new Label() { Text = row["TenDV"].ToString(), Location = new Point(5, 15) ,ForeColor=Color.DarkGreen, Font = new Font("Times New Roman", 12.0f) };
                Label gia = new Label() { Text = row["Giatien"].ToString() + "Đ" , Location = new Point(130, 15), ForeColor = Color.Red, Font = new Font("Times New Roman", 12.0f) };
                btn.Controls.Add(tendv);
                btn.Controls.Add(gia);
                btn.Click += Btn_Click;
                flowLayoutPanel1.Controls.Add(btn);
                btn.Tag = row;
            }
        }

        private void Btn_Click(object sender, EventArgs e)
        {
         
            DataRow row = ((sender as Button).Tag as DataRow);
            dichvu = row["MaDV"].ToString();
            giatien = row["Giatien"].ToString();
            txtGoi.Text = "Đã chọn GóiDV : " + row["TenDV"].ToString();
            
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {

            if (txtten.Text == "" || txtDiaChi.Text == "" || txtSDT.Text == "" || txtGoi.Text == "" || txtdichvu.Text == "")
            {
                MessageBox.Show("Vui lòng nhập,chọn đủ các trường");
            }
            else

            {
                DateTime a = DateTime.Now;

                if (txtDiaChi.Text == "Hà Nội" || txtDiaChi.Text == "Ha Noi" || txtDiaChi.Text == "Ho Chi Minh" || txtDiaChi.Text == "Hồ Chí Minh")
                {
                    DateTime b = a.AddMonths(int.Parse(sothang) + 1);
                    string query = "insert dbo.BangKH values(N'" + txtten.Text + "',N'" + txtDiaChi.Text + "'" +
                   ",'" + txtSDT.Text + "'," + dichvu + "," + goi + ",'" + a.ToString("yyyy/MM/dd") + "','" + b.ToString("yyyy/MM/dd") + "')";
                    m.ThucThiSQLTheoPKN(query);
                    string c = m.scalarReturn("select Max(MaKH) from BangKH");
                    string sotiennop = ((int.Parse(giatien)) * (int.Parse(sothang))).ToString();
                    string query2 = "insert dbo.BangHD values('" + c + "','" + a.ToString("yyyy/MM/dd") + "','" + sotiennop + "')";
                    m.ThucThiSQLTheoPKN(query2);

                    MessageBox.Show("Đăng kí thành công");
                }

                else {

                    DateTime b = a.AddMonths(int.Parse(sothang));
                    string query = "insert dbo.BangKH values(N'" + txtten.Text + "',N'" + txtDiaChi.Text + "'" +
                        ",'" + txtSDT.Text + "'," + dichvu + "," + goi + ",'" + a.ToString("yyyy/MM/dd") + "','" + b.ToString("yyyy/MM/dd") + "')";
                    m.ThucThiSQLTheoPKN(query);
                    string c = m.scalarReturn("select Max(MaKH) from BangKH");
                    string sotiennop = ((int.Parse(giatien)) * (int.Parse(sothang))).ToString();
                    string query2 = "insert dbo.BangHD values('" + c + "','" + a.ToString("yyyy/MM/dd") + "','" + sotiennop + "')";
                    m.ThucThiSQLTheoPKN(query2);

                    MessageBox.Show("Đăng kí thành công"); }
            }
        
            txtten.Clear();
            txtDiaChi.Clear();
            txtSDT.Clear();
        }

        private void LoadGoidich()
        {

            DataTable dt2 = new DataTable();
            dt2.Clear();
            dt2 = m.Laybangtb("select * from Bangkieunoptruoc",dt2);
            
            foreach(DataRow row2 in dt2.Rows)
            {
                Button bm = new Button() { Name = row2["Makieunop"].ToString(),Width=300, Height = 70, BackColor = Color.WhiteSmoke };
                Label tengoi = new Label() { Text = row2["TenGoi"].ToString(), Location = new Point(5, 10), ForeColor = Color.DarkGreen, Font = new Font("Times New Roman", 12.0f) };
                Label Sothang = new Label() { Text ="Số tháng sử dụng: " + row2["Sothang"].ToString(),Width=250, Location = new Point(5, 40), ForeColor = Color.DarkGreen, Font = new Font("Times New Roman", 12.0f) };
                bm.Controls.Add(tengoi);
                bm.Controls.Add(Sothang);
                bm.Tag = row2;
                bm.Click += Bm_Click;
                flowLayoutPanel2.Controls.Add(bm);
            }
        }

        private void Bm_Click(object sender, EventArgs e)
        {
            DataRow row2 = ((sender as Button).Tag as DataRow);
            {
                goi = row2["Makieunop"].ToString();
                sothang = row2["Sothang"].ToString();
                txtdichvu.Text = "Đã chọn: " + row2["TenGoi"].ToString();
            }
        }
    }
}
