﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QL_ITN
{
    public partial class DangNhap : Form
    {
        public DangNhap()
        {
            InitializeComponent();
            checkBox1.Checked = true;
            UserName.Text = Properties.Settings.Default.Username;
            PassWord.Text = Properties.Settings.Default.Password;
        }

        private void buttonEdit1_EditValueChanged(object sender, EventArgs e)
        {

        }
        clsDatabase m = new clsDatabase();
        //Hàm đăng nhập
        private void simpleButton1_Click(object sender, EventArgs e)
        {
            string user, pass;
            user = m.scalarReturn("select Username from Dangnhap where Username ='" + UserName.Text + "'");
            pass = m.scalarReturn("select Matkhau from Dangnhap where Username='" + UserName.Text + "' and Matkhau='" + PassWord.Text + "'");
            if(!(user.Equals(UserName.Text)))
            {
                MessageBox.Show("Username không tồn tại");
            }
            else
                if(pass.Equals(PassWord.Text))
            {
                string roles = m.scalarReturn("select Quyenhan from Dangnhap where Username='" + UserName.Text + "'");
                string tennv = m.scalarReturn("select Tennhanvien from Dangnhap where Username='" + UserName.Text + "'");
                Form1.role = roles;
                Form1.tennv = tennv;
                Form1.username = user;
                Form1 n = new Form1();
                if (checkBox1.Checked)
                {
                    Properties.Settings.Default.Username = UserName.Text;
                    Properties.Settings.Default.Password = PassWord.Text;
                    Properties.Settings.Default.Save();
                }
                else
                {
                    Properties.Settings.Default.Username = "";
                    Properties.Settings.Default.Password = "";
                    Properties.Settings.Default.Save();
                }
                this.Hide();
                n.ShowDialog();
                
                
            }
            else
            {
                MessageBox.Show("Sai mật khẩu");
            }
        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void UserName_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void PassWord_TextChanged(object sender, EventArgs e)
        {
          
        }
        //Click đổi màu textbox
        private void UserName_Click(object sender, EventArgs e)
        {
            UserName.Clear();
            pictureBox1.BackgroundImage = Properties.Resources.iconfinder_user_285655;
            label1.ForeColor = Color.FromArgb(78, 104, 206);
            UserName.ForeColor = Color.FromArgb(78, 104, 206);

            pictureBox2.BackgroundImage = Properties.Resources.iconfinder_lock_icon_211089;
            label1.ForeColor = Color.WhiteSmoke;
            PassWord.ForeColor = Color.WhiteSmoke;
        }
        //Click đổi màu textbox
        private void PassWord_Click(object sender, EventArgs e)
        {
            PassWord.Clear();
            pictureBox2.BackgroundImage = Properties.Resources.iconfinder_lock_icon_211089;
            label1.ForeColor = Color.FromArgb(78, 104, 206);
            PassWord.ForeColor = Color.FromArgb(78, 104, 206);

            pictureBox1.BackgroundImage = Properties.Resources.iconfinder_user_285655;
            label1.ForeColor = Color.WhiteSmoke;
            UserName.ForeColor = Color.WhiteSmoke;
        }
    }
}
