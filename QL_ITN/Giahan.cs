﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QL_ITN
{
    public partial class Giahan : Form
    {
        public Giahan()
        {
            InitializeComponent();
            m.loadcombobox(comboBox1, "select * from BangKH", "TenKH");
            comboBox2.SelectedIndex = 0;
            comboBox1.SelectedIndex = 0;
        }
        clsDatabase m = new clsDatabase();

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            try
            {


                DateTime a = DateTime.Now;
                string maKH = m.scalarReturn("select MaKH from BangKH where TenKH=N'" + comboBox1.Text + "'");
                string Ngaykt = m.scalarReturn("select Ngayketthuc from BangKH where TenKH=N'" + comboBox1.Text + "'");
                string Price = m.scalarReturn("select Giatien from BangKH a,Banggoidichvu b where a.MaKH=" + maKH + " and a.GoiDV=b.MaDV");
                DateTime dt;
               
                dt = DateTime.Parse(Ngaykt);
                dt = dt.AddMonths(int.Parse(comboBox2.Text));
                string query = "update dbo.BangKH set Ngayketthuc='" + dt.ToString("yyyy/MM/dd") + "' where MaKH='" + maKH + "'";
                m.ThucThiSQLTheoPKN(query);
                string Total = ((int.Parse(Price)) * (int.Parse(comboBox2.Text))).ToString();
                string query2 = "insert dbo.BangHD values('" + maKH + "','" + a.ToString("yyyy/MM/dd") + "'," + Total + ")";
                m.ThucThiSQLTheoPKN(query2);
                this.Hide();
            }
            catch
            {
                MessageBox.Show("Lỗi");
            }
        }
    }
}
